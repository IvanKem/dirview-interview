#include <QtGui>
#include <QApplication>
#include <QDesktopWidget>
#include <QFileSystemModel>
#include <QFileIconProvider>
#include <QTreeView>
#include <QLineEdit>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <iostream>
#include <QVBoxLayout>


class FileFilterProxyModel : public QSortFilterProxyModel {
public:
    FileFilterProxyModel(QObject *parent = nullptr)
        : QSortFilterProxyModel(parent) {}

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override {
        QModelIndex index = sourceModel()->index(source_row, 0, source_parent);


        // Get the file name
        QString fileName = sourceModel()->data(index, Qt::DisplayRole).toString();

        // Get the filter text from the QLineEdit
        QString filterText = filterLineEdit->text();

        // Perform the comparison and return true if the name matches the filter
        return fileName.contains(filterText, Qt::CaseInsensitive);
    }

public:
    void setFilterLineEdit(QLineEdit *lineEdit) {
        filterLineEdit = lineEdit;
    }

private:
    QLineEdit *filterLineEdit;
};


int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    QCommandLineParser parser;
    parser.setApplicationDescription("Qt Dir View Example");
    parser.addHelpOption();
    parser.addVersionOption();
    QCommandLineOption dontUseCustomDirectoryIconsOption("c", "Set QFileIconProvider::DontUseCustomDirectoryIcons");
    parser.addOption(dontUseCustomDirectoryIconsOption);

    parser.process(app);

    QFileSystemModel model;
    //Set home dir
    model.setRootPath("home/");
    //show all files and dirs
    model.setFilter(QDir::AllDirs |
                    QDir::Hidden |
                    QDir::Files |
                    QDir::NoDotAndDotDot);



    QTreeView tree;
    //add QLineEdit
    QLineEdit edit(&tree);
    QVBoxLayout mainLayout(&tree);


    //Set widget QLineEdit in TopRightCorner
    mainLayout.addWidget(&edit, 0, Qt::AlignTop | Qt::AlignRight);
    tree.setModel(&model);

    /* Здесь попытка сдеалать фильтрацию файлов и папок через вспомогательный класс унаследованный от QSortFilterProxyModel
        FileFilterProxyModel proxyModel;
        proxyModel.setSourceModel(&model);
        tree.setModel(&proxyModel);

        // Set the filter input field in the filtering model
        proxyModel.setFilterLineEdit(&edit);

        // Handler for text changes in the filter input field
        QObject::connect(&edit, &QLineEdit::textChanged, [&proxyModel]() {
            proxyModel.invalidate(); // Update the filtering model
        });
       */



    /*Еще попытка сделать фильртрацию с помощью встроеных фильтров QFileSystemModel
      QObject::connect(&filterLineEdit, &QLineEdit::textChanged, [&model, &filterLineEdit]() {
        QString filter = filterLineEdit.text();
        model.setNameFilter(filter);

    });*/



    //Set home-user dir
    tree.setRootIndex(model.index(std::getenv("HOME")));


    // Demonstrating look and feel features
    tree.setAnimated(false);
    tree.setIndentation(20);
    //tree.setSortingEnabled(true);
    const QSize availableSize = QApplication::desktop()->availableGeometry(&tree).size();
    tree.resize(availableSize / 2);
    tree.setColumnWidth(0, tree.width() / 3);

    tree.setWindowTitle(QObject::tr("Dir View"));
    tree.show();


    return app.exec();
}
